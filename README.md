gulfscei.rails.server
=========

A role to configure some common items needed to get a rails server up and running

Requirements
------------

None.

Role Variables
--------------

```
app_name: rails
domain_name: localhost

nginx_unix_socket: ''

nginx_ssl_key: ''
nginx_ssl_cert: ''
nginx_dhparam: ''
```

Dependencies
------------

None

Requirements File
-----------------

```
- src: git@gitlab.com:TridentUno/ansible-role-railsserver.git
  scm: git
  version: v0.2
  name: gulfscei.rails.server
```


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: gulfscei.rails.server }

License
-------

MIT

Author Information
------------------

Contact `drward3@uno.edu`
